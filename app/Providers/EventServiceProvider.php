<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Authenticated' => [
            'App\Listeners\LastLoginListener',
        ],
        "App\Modules\Forum\Events\TopicCreated" => [
            "App\Modules\Forum\Listeners\LastPostTopicListener",
            "App\Modules\Forum\Listeners\TopicTapListener"
        ],
        "App\Modules\Forum\Events\TopicWasDeleted" => [
            "App\Modules\Forum\Listeners\LastPostDeletedListener",
        ],
        "App\Modules\Forum\Events\CommentWasDeleted" => [
            "App\Modules\Forum\Listeners\PostTapListener",
        ],
        "App\Modules\Forum\Events\TopicAnswered" => [
            "App\Modules\Forum\Listeners\PostTapListener",
        ],
        "App\Modules\Forum\Events\TopicWasMoved" => [
            "App\Modules\Forum\Listeners\LastPostMovedTopicListener",
            "App\Modules\Forum\Listeners\MovedTopicCounterListener",
        ],
        "Illuminate\Auth\Events\Registered" => [
            "App\Listeners\ValidationListener"
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
