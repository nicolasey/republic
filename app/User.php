<?php

namespace App;

use App\Modules\Bank\Models\Traits\HasAccount;
use App\Modules\Family\Traits\HasFamily;
use App\Traits\SlugTrait;
use App\Traits\ValidationTrait;
use ChristianKuri\LaravelFavorite\Traits\Favoriteability;
use Cog\Ban\Traits\HasBans;
use Cog\Ban\Contracts\HasBans as BanContract;
use Gstt\Achievements\Achiever;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mpociot\Teamwork\Traits\UserHasTeams;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class User extends Authenticatable implements BanContract
{
    use Notifiable, Favoriteability, Achiever, HasRolesAndAbilities, SoftDeletes, HasFamily, SlugTrait, UserHasTeams, HasAccount, HasBans, ValidationTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
