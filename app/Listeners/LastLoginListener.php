<?php
namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Authenticated;

class LastLoginListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Authenticated  $event
     * @return void
     */
    public function handle(Authenticated $event)
    {
        if(isset($event->user->logged_at)) $event->user->last_login = $event->user->logged_at;
        $event->user->logged_at = Carbon::now();
        $event->user->save();
    }
}