<?php
namespace App\Listeners;

use App\Mail\UserValidation;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Mail;

class ValidationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $user = User::find($event->user->id);
        $user->validation_token = str_random(30);
        $user->save();

        Mail::to($user)->send(new UserValidation($user));
    }
}