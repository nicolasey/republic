<?php

namespace App\Http\Controllers;

use App\User;

class ValidationController extends Controller
{
    public function verifEmail($token)
    {
        $user = User::where("validation_token", $token)->firstOrFail();
        $user->validate();

        session()->flash("success", "Votre personnage est validé !");
        return redirect()->route("login");
    }
}
