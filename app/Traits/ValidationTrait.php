<?php
namespace App\Traits;

trait ValidationTrait
{
    public function validate()
    {
        $this->active = true;
        $this->validation_token = null;
        $this->save();
    }
}