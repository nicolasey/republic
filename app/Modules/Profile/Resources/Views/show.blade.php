@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <img src="{{ asset("uploads/avatars/".$user->avatar) }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
                <h2>{{ $user->name }}</h2>
                <h4>{{ $user->points }}</h4>
                @can("avatar.update", $user)
                    <form enctype="multipart/form-data" action="{{ route("avatar.update") }}" method="POST">
                        <label>Changer d'avatar</label>
                        <input type="file" name="avatar">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" class="pull-right btn btn-sm btn-primary">
                    </form>
                @endcan
            </div>
        </div>
    </div>
@endsection