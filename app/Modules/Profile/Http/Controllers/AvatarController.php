<?php

namespace App\Modules\Profile\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Auth;

class AvatarController extends Controller
{
    public function update(Request $request)
    {
        // Handle the user upload of avatar
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename ) );

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();

            // flash("Avatar mis à jour avec succès", "success");
            return redirect()->route("profile.show", ["slug" => $user->slug]);
        }
        // flash("Aucun avatar, rien du tout", "error");
        return redirect()->back();
    }
}
