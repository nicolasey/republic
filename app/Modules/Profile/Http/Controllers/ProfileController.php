<?php

namespace App\Modules\Profile\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    /**
     * Show user profile
     *
     * @param null $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug = null)
    {
        $user = ($slug) ? User::where("slug", $slug)->firstOrFail() : Auth::user();
        return view("profile::show", compact("user"));
    }
}
