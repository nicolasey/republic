<?php

namespace App\Modules\Family\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Family\Models\Family;
use Illuminate\Support\Facades\Auth;
use App\User;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use App\Modules\Family\Http\Requests\FamilyGiveRequest;

class FamilyController extends Controller
{
    /**
     * Add a user to a family
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add($id)
    {
        $family = Family::findOrFail($id);
        return view("family::add", compact("family"));
    }

    /**
     * Link a user to a users family
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function link($id, Request $request)
    {
        $family = Family::findOrFail($id);
        if(Auth::attempt(["name" => $request->input('username'), "password" => $request->input("password")], true))
        {
            $user = Auth::user();

            if(isset($user->family_id)) return redirect()->route("family.show", $user->slug);

            $user->family_id = $family->id;
            $user->save();

            return redirect()->route("family.show", $user->slug);
        }
        return redirect()->route("home");
    }

    /**
     * View a family
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug = null)
    {
        $user = ($slug) ? User::where("slug", $slug)->firstOrFail() : Auth::user();

        if(!isset($user->family_id)) {
            $family = Family::create();
            $user->family_id = $family->id;
            $user->save();
            $family = Family::with("users")->findOrFail($family->id);
        } else {
            $family = Family::with("users")->findOrFail($user->family_id);
        }

        return view("family::show", compact("family"));
    }

    /**
     * Retirer un PJ de la liste
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(User $user)
    {
        if($user->isAn("admin") or (Auth::user()->family_id == $user->family_id))
        {
            $user->family_id = null;
            $user->save();
        }
        return redirect()->back();
    }

    /**
     * Put a user on sleep or awake it
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sleep(User $user)
    {
        if(Auth::user()->isAn("admin") or (Auth::user()->family_id == $user->family_id))
        {
            $user->sleep = ! $user->sleep;
            $user->save();
            return redirect()->back();
        }
        else
        {
            return redirect()->back();
        }
    }

    /**
     * Change current user
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function shift($id)
    {
        $user = User::findOrFail($id);
        if(Auth::user()->family_id == $user->family_id)
        {
            Auth::loginUsingId($user->id);
            return redirect()->back();
        }
        else
        {
            throw new AccessDeniedException("L'utilisateur demandé n'est pas de la même famille");
        }
    }

    /**
     * Next release
     */
    public function give(FamilyGiveRequest $request)
    {
        if(! config("family.enable_give"))
        {
            return redirect()->back();
        }

        $given = User::firstOrFail($request->input("given_id"));
        $receiver = User::firstOrFail($request->input("receiver_id"));

        // the user must be authorized to give a user

        // given user must not be locked - if locked, it is not moveable
        if($given->locked and (!Auth::user()->isAn("admin")))
        {
            return redirect()->back();
        }

        // receiver must respect user limitations (in config)
        $family = $this->getFamily($receiver);
        if($family->countPJ() >= config("family.max_pj") and !($family->unlimited))
        {
            return redirect()->back();
        }

        // perform transmission
        $given->family_id = $family->id;
        $given->save();

        return redirect()->back();
    }
}
