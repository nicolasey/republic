<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(["middleware" => "web", "prefix" => "family"], function () {
    Route::get("/delete/{id}", ["uses" => "FamilyController@delete", "as" => "family.delete"]);
    Route::get("/sleep/{id}", ["uses" => "FamilyController@sleep", "as" => "family.sleep"]);
    Route::get("/shift/{id}", ["uses" => "FamilyController@shift", "as" => "family.shift"]);
    Route::get("/{slug?}", ["uses" => "FamilyController@show", "as" => "family.show"]);
    Route::get("/{id}/add", ["uses" => "FamilyController@add", "as" => "family.add"]);
    Route::post("/{id}/add", ["uses" => "FamilyController@link", "as" => "family.link"]);
});