<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("families", function(Blueprint $table){
            $table->increments('id');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table("users", function (Blueprint $table){
            $table->unsignedMediumInteger("family_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("families");
        Schema::table("users", function (Blueprint $table) {
            $table->dropColumn(['family_id']);
        });
    }
}
