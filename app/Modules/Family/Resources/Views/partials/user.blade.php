<div class="row user-row">
    <div class="col-md-1 col-xs-1 col-sm-1">
        <img class="avatar"
             src="{{ asset("uploads/avatars/".$pj->avatar) }}"
             alt="{{ $pj->name }} Pic">
    </div>
    <div class="col-md-8 col-xs-10 col-xs-offset-1 col-sm-10">
        <strong>{{ $pj->name }}</strong><br>
        <span class="text-muted">Maître Jedi</span>
        @if($pj->isAn("admin"))
            <span class="label label-default">Admin</span>
        @endif
        @if($pj->isA("mj"))
            <span class="label label-warning">MJ</span>
        @endif
    </div>
</div>