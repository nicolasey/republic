<div class="divider"></div>
@if(Auth::user()->family_id)
    @foreach(Auth::user()->family->users as $pj)
        @if(Auth::user()->id != $pj->id)
            <li>
                <a href="{{ route("family.shift", ["id" => $pj->id]) }}" role="button">
                    <img src="{{ asset("uploads/avatars/".$pj->avatar) }}" style="width:32px; height:32px; margin:0; border-radius:50%">
                    {{ $pj->name }}
                </a>
            </li>
        @endif
    @endforeach
@endif
<li><a href="{{ route("family.show", ["slug" => null]) }}">Gérer mes PJ</a></li>
<div class="divider"></div>