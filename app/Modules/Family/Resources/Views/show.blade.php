@extends("layouts.app")

@section("content")
    <div class="container family">
        <div class="well col-sm-12 col-md-8 col-md-offset-2">
            @foreach($family->users as $pj)
                @include("family::partials.user")
            @endforeach
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="text-center">
                <a href="{{ route("family.add", ["id" => $family->id]) }}">Ajouter un PJ</a>
            </div>
        </div>
    </div>
@endsection