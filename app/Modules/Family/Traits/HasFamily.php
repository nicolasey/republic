<?php
namespace App\Modules\Family\Traits;

use App\Modules\Family\Models\Family;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasFamily
{
    /**
     * Family relation
     * @return BelongsTo
     */
    public function family()
    {
        return $this->belongsTo(Family::class);
    }
}