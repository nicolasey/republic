<?php

namespace App\Modules\Family\Models;

use App\Modules\Bank\Models\Account;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Family extends Model
{
    use SoftDeletes;

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function countPJ()
    {
        return $this->users()
            ->filter(function ($model) {
                return ((! $model->staff) and (! $model->sleep));
            })
            ->count();
    }

    public function activePJ()
    {
        return $this->users()
            ->filter(function ($model) {
                return (! $model->sleep);
            });
    }
}
