<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'forum', "middleware" => "web"], function () {
    Route::group(["prefix" => "p"], function () {
        Route::get("/{id}", ["uses" => "CommentController@to", "as" => "comment.to"]);
        Route::get("/{id}/delete", ["uses" => "CommentController@delete", "as" => "comment.delete"]);
        Route::get("/{id}/lock", ["uses" => "CommentController@lock", "as" => "comment.lock"]);
        Route::post("/{id}", ["uses" => "CommentController@update", "as" => "comment.update"]);
        Route::get("/{id}/editer", ["uses" => "CommentController@edit", "as" => "comment.edit"]);
    });

    Route::group(["prefix" => "t"], function () {
        Route::get("/{slug}", ["uses" => "TopicController@show", "as" => "discussion.show"]);
        Route::post("/", ["uses" => "TopicController@store", "as" => "discussion.store"]);
        Route::post("/{slug}", ["uses" => 'TopicController@update', "as" => "discussion.update"]);
        Route::get("/{slug}/editer", ["uses" => 'TopicController@edit', "as" => "discussion.edit"]);
        Route::get("/{slug}/repondre", ["uses" => 'CommentController@create', "as" => "comment.create", "middleware" => "auth"]);
        Route::post("/{slug}/repondre", ["uses" => 'CommentController@store', "as" => "comment.store", "middleware" => "auth"]);
    });

    Route::get("/", ["uses" => 'ForumController@index', "as" => "forum.index"]);

    Route::post("/new", ["uses" => "ForumController@store", "as" => "forum.store"]);
    Route::get("/{slug}", ["uses" => 'ForumController@show', "as" => "forum.show"]);
    Route::get("/creer/{slug?}", ["uses" => "ForumController@create", "as" => "forum.create"]);
    Route::post("/{id}/{comment_id}", ["uses" => 'ForumController@update', "as" => "forum.update"]);
    Route::get("/{slug}/editer", ["uses" => 'ForumController@edit', "as" => "forum.edit"]);
    Route::get("/{slug}/lock", ["uses" => 'ForumController@lock', "as" => "forum.lock"]);
    Route::get("/{slug}/supprimer", ["uses" => 'ForumController@delete', "as" => "forum.delete"]);
    Route::get("/{slug}/creer-un-topic", ["uses" => "TopicController@create", "as" => "discussion.create"]);
});
