<?php

namespace App\Modules\Forum\Http\Controllers;

use App\Modules\Forum\Events\TopicCreated;
use App\Modules\Forum\Events\TopicWasDeleted;
use App\Modules\Forum\Events\TopicWasMoved;
use App\Modules\Forum\Models\Channel;
use App\Modules\Forum\Models\Comment;
use App\Modules\Forum\Models\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TopicController extends Controller
{
    public function create($slug)
    {
        $channel = Channel::where("slug", $slug)->firstOrFail();
        return view("forum::discussion.create", compact("channel"));
    }

    /**
     * Store new topic
     *
     * @todo refactor request to get the validation and access rule out
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if(auth()->guest()) return redirect()->back();
        $this->validate($request, [
            "discussion_title" => "required|min:3",
            "posting" => "required|min:3",
            "category_id" => "required"
        ]);

        $discussion = Topic::create([
            "name" => $request->input("discussion_title"),
            "user_id" => auth()->user()->id,
            "sticky" => false,
            "channel_id" => $request->input("category_id")
        ]);

        $comment = Comment::create([
            "body" => $request->input("posting"),
            "user_id" => auth()->user()->id,
            "commentable_id" => $discussion->id,
            "commentable_type" => Topic::class,
            "position" => 1
        ]);

        $discussion->last_post = $comment->id;
        $discussion->save();

        event(new TopicCreated($discussion));

        return redirect()->route("discussion.show", ["slug" => $discussion->slug]);
    }

    public function show($slug)
    {
        $discussion = Topic::where("slug", $slug)->firstOrFail();
        $discussion->tapView();
        $comments = Comment::where("commentable_type", Topic::class)
            ->where("commentable_id", $discussion->id)
            ->with("commentable")
            ->with("user")
            ->paginate(10);

        return view("forum::discussion.show", compact("comments", "discussion"));
    }

    /**
     * Edit a topic
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($slug)
    {
        $topic = Topic::where("slug", $slug)->firstOrFail();
        $comment = Comment::where([
            "commentable_type" => Topic::class,
            "commentable_id" => $topic->id
        ])->first();

        return view("forum::discussion.edit", compact("topic", "comment"));
    }

    /**
     * Update a topic
     *
     * @param Request $request
     * @param $id
     * @param $comment_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id, $comment_id)
    {
        $this->validate($request, [
            "posting" => "required|min:6",
            "discussion_title" => "required|min:3"
        ]);

        $topic = Topic::findOrFail($id);
        $comment = Comment::findOrFail($comment_id);

        $topic->title = $request->input("discussion_title");
        $topic->save();

        $comment->body = $request->input("posting");
        $comment->save();

        \Session::flash("success", "Topic mis à jour");
        return redirect()->route("discussion.show", ["slug" => $topic->slug]);
    }

    /**
     * Move a topic to a different channel
     * @param int $id
     * @param int $channel_to
     * @return \Illuminate\Http\RedirectResponse
     */
    public function move($id, $channel_to)
    {
        $topic = Topic::findOrFail($id);
        $before = Channel::findOrFail($topic->channel_id);
        $channel = Channel::findOrFail($channel_to);

        $topic->channel_id = $channel->id;
        $topic->save();

        event(new TopicWasMoved($topic, $before, $channel));

        return redirect()->route("discussion.show", ["slug" => $topic->slug]);
    }

    /**
     * Delete topic
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $topic = Topic::with("channel")->findOrFail($id);
        $topic->delete();

        event(new TopicWasDeleted($topic));
        
        return redirect()->route("forum.show", ["slug" => $topic->channel->slug]);
    }
}
