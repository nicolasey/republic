<?php

namespace App\Modules\Forum\Http\Controllers;

use App\Modules\Forum\Events\CommentWasAmended;
use App\Modules\Forum\Events\CommentWasDeleted;
use App\Modules\Forum\Events\TopicAnswered;
use App\Modules\Forum\Models\Channel;
use App\Modules\Forum\Models\Comment;
use App\Modules\Forum\Models\Topic;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Forum\Helpers\LastPostHelper;
use Illuminate\Support\Facades\Session;

class CommentController extends Controller
{
    /**
     * Answer to a discussion
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($slug)
    {
        $discussion = Topic::where("slug", $slug)->firstOrFail();
        return view("forum::discussion.answer", compact("discussion"));
    }

    /**
     * Store an answer to a discussion
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "posting" => "required|min:3",
            "discussion_id" => "required"
        ]);

        $discussion = Topic::findOrFail($request->input("discussion_id"));
        // We must assess that user is auth to post in this discussion

        $comment = Comment::create([
            "user_id" => auth()->user()->id,
            "commentable_type" => Topic::class,
            "commentable_id" => $discussion->id,
            "body" => $request->input("posting")
        ]);

        // Update last post for discussion
        $discussion->last_post = $comment->id;
        // Update nb of answers in discussion
        $discussion->answers += 1;
        $discussion->save();

        LastPostHelper::setChannelLastPost($discussion->channel, $comment);

        // Set the comment position in discussion
        $comment->position = $discussion->answers + 1;
        $comment->save();

        event(new TopicAnswered($discussion));

        return redirect()->route("discussion.show", ["slug" => $discussion->slug]);
    }

    /**
     * Redirect to a given comment in a discussion
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function to($id)
    {
        $comment = Comment::with("commentable")
            ->findOrFail($id);

        $page = ceil($comment->position / 15);

        // Construct url to the right page, to the right comment (using html anchors)
        $url = \URL::route("discussion.show", ["slug" => $comment->commentable->slug]);
        $url = ($page > 1) ? $url . "?page=" . $page : $url;
        $url = $url . "#post-" . $comment->id;

        return redirect()->to($url);
    }

    public function edit($id)
    {
        /*
         * impossible si :
         * - topic is locked
         * - comment is locked
         * - is nor admin nor owner
         */
        $comment = Comment::findOrFail($id);
        return view("forum::discussion.amend", compact("comment"));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            "posting" => "required|min:6"
        ]);

        $comment = Comment::findOrFail($id);
        $comment->body = $request->input("posting");
        $comment->save();

        event(new CommentWasAmended($comment));

        Session::flash("success", "Post modifié !");
        return redirect()->route("comment.to", ["id" => $comment->id]);
    }

    /**
     * Delete a comment
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $comment = Comment::with("commentable")->findOrFail($id);
        $topic = Topic::findOrFail($comment->commentable->id);

        $comment->delete();
        $topic->answers -= 1;
        $topic->save();

        if ($topic->last_post == $comment->id) {
            LastPostHelper::setTopicLastPost($topic);
            LastPostHelper::setDeleteChannelsLastPost($topic);
        }

        event(new CommentWasDeleted($comment));

        // @todo add flash message with success
        return redirect()->route("discussion.show", [$topic->slug]);
    }

    /**
     * Lock or unlock the comment specifically
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function lock($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->locked = !$comment->locked;
        $comment->save();
        return redirect()->back();
    }

    public function report($id)
    {

    }
}
