<?php

namespace App\Modules\Forum\Http\Controllers;

use App\Modules\Forum\Events\ChannelWasMoved;
use App\Modules\Forum\Models\Channel;
use App\Modules\Forum\Models\Topic;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Forum\Events\ChannelWasDeleted;

class ForumController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $channels = Channel::where("parent_id", null)
            ->with("children")
            ->get();

        $channels = $this->checkViewable($channels);
        foreach ($channels as $channel) {
            if(isset($channel->children)) $channel->children = $this->checkViewable($channel->children);
        }

        return view("forum::index", compact("channels"));
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $channel = Channel::where("slug", $slug)
            ->with("children", "topics")
            ->firstOrFail();

        $channel->ancestors = $channel->getAncestors();

        // check if access is authorized

        if(isset($channel->children)) $channel->children = $this->checkViewable($channel->children);

        $discussions = Topic::where("channel_id", $channel->id)
            ->with("user")
            ->with('lastPost', "lastPost.user")
            ->paginate(15);

        return view("forum::show", compact("channel", "discussions"));
    }

    /**
     * Create channels
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($slug = null)
    {
        $channels = Channel::get();
        $default = $channels->where("slug", $slug)->first();
        return view("forum::create", compact("channels", "default"));
    }

    /**
     * Store created channels
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:channels|max:191',
            'body' => 'required|min:3',
            "parent_id" => "nullable"
        ]);

        $channel = Channel::create([
            "parent_id" => $request->input("parent_id"),
            "name" => $request->input("name"),
            "body" => $request->input("body")
        ]);
        return redirect()->route("forum.show", ["slug" => $channel->slug]);
    }

    public function edit($slug)
    {
        $channels = Channel::get();
        $object = $channels->where("slug", $slug)->first();
        return view("forum::edit", compact("object", "channels"));
    }

    public function update(Request $request, $slug)
    {
        $this->validate($request, [
            'name' => 'required|max:191',
            'body' => 'required|min:3',
            "parent_id" => "nullable"
        ]);

        $channel = Channel::where("slug", $slug)->firstOrFail();

        if($request->input("parent_id") == $channel->id) throw new \Exception("Cannot be parent of itself");
        $hasMoved = ($channel->parent_id != $request->input("parent_id"));

        $channel->name = $request->input("name");
        $channel->body = $request->input("body");
        $channel->parent_id = $request->input("parent_id");
        $channel->save();

        if($hasMoved) event(new ChannelWasMoved($channel));

        return redirect()->route("forum.show", ["slug" => $channel->slug]);
    }

    public function delete($id)
    {
        $channel = Channel::findOrFail($id);
        $redir = $channel->parent->slug;
        $channel->delete();

        event(new ChannelWasDeleted($channel));
        // @todo add flash notif
        return redirect()->route("forum.show", ["slug" => $redir]);
    }

    /**
     * Filter channels to show only viewable occurrences
     * @param Collection $collection
     */
    private function checkViewable(Collection $collection)
    {
        /**
         * Status
         * 0: normal
         * 1: private
         * 2: secret
         *
         * Secret cannot be viewed and are therefore withdrawn from Collection unless ability is present
         * Private status are handled on show method
         * Required ability is defined in status_name in model
         */
        $abilities = (auth()->user()) ? auth()->user()->getAbilities() : null;
        $collection->filter(function ($item) use ($abilities) {
            if(($item->status > 1) and !(auth()->user()->can("view", $item))) return false;
            return true;
        });
        return $collection;
    }
}
