<?php

namespace App\Modules\Forum\Models;

use App\Modules\Forum\Models\Traits\DateTrait;
use App\Modules\Forum\Models\Traits\LastPostTopicTrait;
use App\Modules\Forum\Models\Traits\LockableTrait;
use App\Modules\Forum\Models\Traits\StatsTopicTrait;
use App\Traits\SlugTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{
    use SoftDeletes, SlugTrait, LockableTrait, DateTrait;

    protected $fillable = ["name", "sticky", "user_id", "channel_id"];
    protected $touches = ["channel"];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, "commentable");
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function lastPost()
    {
        return $this->belongsTo(Comment::class, "last_post");
    }

    public function tapView()
    {
        $this->views++;
        $this->save();
    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function ($model) {
            foreach ($model->comments as $comment) $comment->delete();
        });
    }
}
