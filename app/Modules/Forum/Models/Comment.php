<?php

namespace App\Modules\Forum\Models;

use App\Modules\Forum\Models\Traits\DateTrait;
use App\Modules\Forum\Models\Traits\LockableTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Comment extends Model implements AuditableContract
{
    use SoftDeletes, LockableTrait, DateTrait, Auditable;

    protected $fillable = ["body", "user_id", "commentable_id", "commentable_type", "position"];

    public function commentable()
    {
        return $this->morphTo("commentable");
    }

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }
}
