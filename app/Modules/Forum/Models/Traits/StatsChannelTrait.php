<?php
namespace App\Modules\Forum\Models\Traits;


trait StatsChannelTrait
{
    public function tapPostCounter($n = 1)
    {
        $this->nbPosts = $this->nbPosts + $n;
        $this->save();
    }

    public function tapTopicCounter($n = 1)
    {
        $this->nbDiscussions = $this->nbDiscussions + $n;
        $this->save();
    }
}