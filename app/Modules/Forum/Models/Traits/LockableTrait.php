<?php
namespace App\Modules\Forum\Models\Traits;


use App\Modules\Forum\Events\ModelWasLocked;
use App\Modules\Forum\Events\ModelWasUnlocked;

trait LockableTrait
{
    public function lock()
    {
        $this->locked = true;
        $this->save();
        event(new ModelWasLocked($this));
    }

    public function unlock()
    {
        $this->locked = false;
        $this->save();
        event(new ModelWasUnlocked($this));
    }
}