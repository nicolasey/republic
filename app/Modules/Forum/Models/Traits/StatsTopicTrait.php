<?php
namespace App\Modules\Forum\Models\Traits;


trait StatsTopicTrait
{
    public function postCount()
    {
        $result = Comment::where('commentable_id', $this->id)
            ->where("commentable_type", self::class)
            ->whereNull('deleted_at')
            ->count();
        return $result;
    }

    public function resetPostCount()
    {
        $this->answers = $this->postCount();
        $this->save();
    }
}