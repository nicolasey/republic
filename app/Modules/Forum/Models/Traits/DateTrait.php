<?php
namespace App\Modules\Forum\Models\Traits;

use Carbon\Carbon;

trait DateTrait
{
    public function getCreationAtAttribute()
    {
        $value = new Carbon($this->created_at);
        return $value->diffForHumans();
    }

    public function getModifiedAtAttribute()
    {
        $value = new Carbon($this->updated_at);
        return $value->diffForHumans();
    }
}