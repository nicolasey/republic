<?php

namespace App\Modules\Forum\Models;

use App\Modules\Forum\Models\Traits\DateTrait;
use App\Modules\Forum\Models\Traits\LockableTrait;
use App\Modules\Forum\Models\Traits\StatsChannelTrait;
use App\Traits\SlugTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;

class Channel extends Model
{
    use SoftDeletes, DateTrait, SlugTrait, LockableTrait, NodeTrait, StatsChannelTrait;

    protected $touches = ["parent"];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ["body", "name", "parent_id", "avatar"];

    /**
     * Model rules
     * @var array
     */
    public static $rules = [
        "body" => "required",
        "name" => "required|min:3"
    ];

    public function topics()
    {
        return $this->hasMany(Topic::class);
    }

    public function lastPost()
    {
        return $this->belongsTo(Comment::class, "last_post");
    }

    public static function boot()
    {
        parent::boot();

        // Cascade delete
        static::deleted(function ($model) {
            foreach ($model->topics as $topic) $topic->delete();
        });
    }
}
