<?php
namespace App\Modules\Forum\Events;

use App\User;
use App\Modules\Forum\Models\Channel;
use App\Modules\Forum\Models\Topic;
use Illuminate\Queue\SerializesModels;

class TopicWasMoved
{
    use SerializesModels;

    public $topic;
    public $channel;
    public $before;
    public $user;

    public function __construct(Topic $topic, Channel $before, Channel $channel)
    {
        $this->channel = $channel;
        $this->before = $before;
        $this->topic = $topic;
        $this->user = auth()->user();
    }
}