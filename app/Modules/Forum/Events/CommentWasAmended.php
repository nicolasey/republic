<?php
namespace App\Modules\Forum\Events;

use App\Modules\Forum\Models\Comment;
use Illuminate\Queue\SerializesModels;

class CommentWasAmended
{
    use SerializesModels;

    public $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
        $this->user = auth()->user();
    }
}