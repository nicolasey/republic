<?php
namespace App\Modules\Forum\Events;

use App\Modules\Forum\Models\Topic;
use App\User;
use Illuminate\Queue\SerializesModels;

class TopicAnswered
{
    use SerializesModels;

    public $topic;
    public $user;

    public function __construct(Topic $topic)
    {
        $this->topic = $topic;
        $this->user = auth()->user();
    }
}