<?php
namespace App\Modules\Forum\Events;

use App\Modules\Forum\Models\Channel;
use Illuminate\Queue\SerializesModels;

class ChannelWasDeleted
{
    use SerializesModels;

    public $channel;
    public $user;

    public function __construct(Channel $channel)
    {
        $this->channel = $channel;
        $this->user = auth()->user();
    }
}