<?php
namespace App\Modules\Forum\Events;

use App\User;
use App\Modules\Forum\Models\Topic;
use Illuminate\Queue\SerializesModels;

class TopicWasDeleted
{
    use SerializesModels;

    public $topic;
    public $user;

    public function __construct(Topic $topic)
    {
        $this->topic = $topic;
        $this->user = auth()->user();
    }
}