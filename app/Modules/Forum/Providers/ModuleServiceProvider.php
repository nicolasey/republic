<?php

namespace App\Modules\Forum\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'forum');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'forum');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'forum');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config.php', 'forum'
        );
        $this->app->register(RouteServiceProvider::class);
    }
}
