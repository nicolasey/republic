<?php
namespace App\Modules\Forum\Listeners;


use App\Modules\Forum\Events\TopicWasDeleted;
use App\Modules\Forum\Helpers\LastPostHelper;

class LastPostDeletedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TopicWasDeleted  $event
     * @return void
     */
    public function handle($event)
    {
        LastPostHelper::setDeleteChannelsLastPost($event->topic);
    }
}