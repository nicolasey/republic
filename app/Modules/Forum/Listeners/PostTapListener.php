<?php
namespace App\Modules\Forum\Listeners;


use App\Modules\Forum\Events\CommentWasDeleted;
use App\Modules\Forum\Events\TopicAnswered;

class PostTapListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TopicAnswered|CommentWasDeleted  $event
     * @return void
     */
    public function handle($event)
    {
        $channel = Channel::findOrFail($event->topic->channel_id);

        (is_a($event, TopicAnswered::class)) ? $channel->tapPostCounter() : $channel->tapPostCounter(-1);

        foreach ($channel->getAncestors() as $ancestor)
        {
            (is_a($event, TopicAnswered::class)) ? $ancestor->tapPostCounter() : $ancestor->tapPostCounter(-1);
        }
    }
}