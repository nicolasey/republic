<?php
namespace App\Modules\Forum\Listeners;

use App\Modules\Forum\Events\TopicWasMoved;

class MovedTopicCounterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TopicWasMoved  $event
     * @return void
     */
    public function handle($event)
    {
        $event->before->tapTopicCounter(-1);
        $event->channel->tapTopicCounter();

        $event->before->tapPostCounter(($event->topic->answers +1)* -1);
        $event->channel->tapPostCounter($event->topic->answers +1);
    }
}