<?php
/**
 * Created by PhpStorm.
 * User: nicolasey
 * Date: 17/06/2017
 * Time: 12:13
 */

namespace App\Modules\Forum\Listeners;

use App\Modules\Forum\Events\TopicCreated;
use App\Modules\Forum\Helpers\LastPostHelper;
use App\Modules\Forum\Models\Channel;

class LastPostTopicListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TopicCreated  $event
     * @return void
     */
    public function handle(TopicCreated $event)
    {
        $comment = $event->topic->comments->first();
        $channel = Channel::findOrFail($event->topic->channel_id);
        LastPostHelper::setChannelLastPost($channel, $comment);
    }
}