<?php
namespace App\Modules\Forum\Listeners;


use App\Modules\Forum\Events\TopicWasMoved;
use App\Modules\Forum\Helpers\LastPostHelper;

class LastPostMovedTopicListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TopicWasMoved  $event
     * @return void
     */
    public function handle($event)
    {
        LastPostHelper::setLastPost($event->channel);
        LastPostHelper::setLastPost($event->before);
    }
}