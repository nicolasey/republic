<?php
namespace App\Modules\Forum\Seeds;

use Illuminate\Database\Seeder;
use App\Modules\Forum\Models\Channel;

class ChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $channels = [
            [
                "name" => "Maman"
            ],
            [
                "name" => "Fiston",
                "parent_id" => 1,
            ],
            [
                "parent_id" => 2,
            ],
            [
                "parent_id" => 2,
            ],
            [
                "parent_id" => 2,
            ],
        ];

        foreach ($channels as $channel) {
            factory(Channel::class)->create($channel);
        }

        factory(Channel::class, 5)->create([
            "parent_id" => rand(2, 5)
        ]);

    }
}