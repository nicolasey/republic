<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("topics", function (Blueprint $table) {
            $table->increments("id");
            $table->timestamps();
            $table->softDeletes();

            $table->string("name");
            $table->string("slug");
            $table->boolean("sticky")->default(false);
            $table->unsignedInteger("views")->default(0);
            $table->unsignedInteger("answers")->default(0);

            $table->unsignedMediumInteger("channel_id")->default(1);
            $table->unsignedMediumInteger("user_id");
            $table->unsignedMediumInteger("last_post")->nullable();
            $table->boolean("locked")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("topics");
    }
}
