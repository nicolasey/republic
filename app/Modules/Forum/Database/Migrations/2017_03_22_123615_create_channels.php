<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateChannels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("channels", function (Blueprint $table) {
            $table->timestamps();
            $table->softDeletes();
            $table->increments("id");

            NestedSet::columns($table);
            $table->unsignedMediumInteger("last_post")->nullable();
            $table->boolean("locked")->default(false);

            $table->string("name");
            $table->string("slug");
            $table->text("body")->nullable();
            $table->string("avatar")->default("default.png");

            $table->unsignedMediumInteger("nbDiscussions")->default(0);
            $table->unsignedMediumInteger("nbPosts")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("channels");
    }
}
