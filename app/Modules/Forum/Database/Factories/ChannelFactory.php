<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Modules\Forum\Models\Channel::class, function (Faker\Generator $faker) {
    static $parent_id;
    static $name;

    return [
        'name' => $name ?: $name = $faker->name,
        'body' => $faker->paragraph(3, true),
        'parent_id' => $parent_id ?: null,
    ];
});
