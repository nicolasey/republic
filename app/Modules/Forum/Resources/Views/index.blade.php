@extends("layouts.app")

@section("content")
    <div class="container forum">

        @foreach($channels as $channel)
            @include("forum::parts.category")
        @endforeach

        @unless(empty($channels))
            @include("forum::parts.no_channel")
        @endunless
    </div>
@endsection