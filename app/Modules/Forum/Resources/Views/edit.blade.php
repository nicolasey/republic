@extends("layouts.app")

@section("content")
    <div class="container forum">

        <form class="form-horizontal" role="form" action="{{ route("forum.update", ["slug" => $object->slug]) }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <legend>Créer une catégorie ou un forum</legend>

            <div class="form-group">
                <label class="control-label" for="name">Nom</label>
                <input type="text" name="name" class="form-control" maxlength="191" value="{{ $object->name }}">
            </div>

            <div class="form-group">
                <label for="channel" class="control-label">Forum parent</label>
                <select name="parent_id" id="channel">
                    <option value="">Pas de parent</option>
                    @foreach($channels as $channel)
                        @if($channel->id != $object->id)
                        <option value="{{ $channel->id }}" @if($object->parent_id == $channel->id) selected @endif>{{ $channel->name }}</option>
                        @endif
                    @endforeach
                </select>
                <p><i>Les forums prennent les paramètres de leur forum parent</i></p>
            </div>

            <div class="form-group">
                <label class="control-label">Description </label>
                <textarea name="body" class="form-control" rows="3">{{ $object->body }}</textarea>
            </div>

            <div class="text-center">
                <button class="btn btn-primary" type="submit">C'est parti !</button>
            </div>
        </form>

    </div>
@endsection