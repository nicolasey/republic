@foreach($channel->ancestors as $ancestor)
    <li><a href="{{ route("forum.show", ['slug' => $ancestor->slug]) }}">{{ $ancestor->name }}</a></li>
@endforeach