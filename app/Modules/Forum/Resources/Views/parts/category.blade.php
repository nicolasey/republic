<table class="table forum table-striped">
    <thead>
    <tr>
        <th class="cell-stat"></th>
        <th>
            <h3>{{ strtoupper($channel->name) }}</h3>
        </th>
        <th class="cell-stat text-center hidden-xs hidden-sm">Topics</th>
        <th class="cell-stat text-center hidden-xs hidden-sm">Posts</th>
        <th class="cell-stat-2x hidden-xs hidden-sm">Dernier Post</th>
    </tr>
    </thead>
    <tbody>
    @if($channel->children->count() > 0)
        @foreach($channel->children as $child)
            @include("forum::parts.forum")
        @endforeach
    @else
        @include("forum::parts.no_forum")
    @endif
    </tbody>
</table>