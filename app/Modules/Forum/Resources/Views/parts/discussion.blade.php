<tr>
    <td class="text-center"><i class="fa fa-heart fa-2x text-primary"></i></td>
    <td>
        <h4>
            <a href="{{ route("discussion.show", ['slug' => $discussion->slug]) }}">{{ $discussion->name }}</a>
            <br><small>par {{ $discussion->user->name }} - {{ $discussion->creation_at }}</small>
        </h4>
    </td>
    <td class="text-center hidden-xs hidden-sm">{{ $discussion->answers }}</td>
    <td class="text-center hidden-xs hidden-sm">{{ $discussion->views }}</td>
    <td class="hidden-xs hidden-sm">
        <small><a href="{{ route("comment.to", ["id" => $discussion->lastPost->id]) }}">{{ $discussion->lastPost->commentable->name }}</a></small><br>
        par <a href="#">{{ $discussion->lastPost->user->name }}</a>
        <br><small><i class="fa fa-clock-o"></i> {{ $discussion->lastPost->creation_at }}</small>
    </td>
</tr>