<li class="media well post" id="post-{{ $comment->id }}">
    <div class="pull-left user-info">
        <img class="avatar img-circle img-thumbnail" src="{{ asset("uploads/avatars/".$comment->user->avatar) }}"
             width="64" alt="user {{ $comment->user->name }}">
        <strong><a href="#">{{ $comment->user->name }}</a></strong>
        <small>Member</small>
    </div>
    <div class="pull-right post__actions">
        <div class="forum-post-panel btn-group btn-group-xs">
            <a href="{{ route("comment.delete", ["id" => $comment->id]) }}" class="btn btn-warning"><i class="fa fa-warning"></i> Delete post</a>
            <a href="#" class="btn btn-danger"><i class="fa fa-warning"></i> Report post</a>
            <a href="#" class="btn btn-default"><i class="fa fa-clock-o"></i> {{ $comment->creation_at }}</a>
        </div>
    </div>
    <div class="media-body">
        <div class="post__body">
            {!! BBCode::parse($comment->body) !!}
        </div>
    </div>
</li>

