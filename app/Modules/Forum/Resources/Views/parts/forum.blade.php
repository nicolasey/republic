<tr>
    <td class="text-center">
        <img src="{{ asset("uploads/categories/".$child->avatar) }}" alt="{{ $child->name }}" class="forum__avatar">
    </td>
    <td>
        <h4>
            <a href="{{ route("forum.show", ['slug' => $child->slug]) }}">{{ $child->name }}</a>
            <br><small>{{ $child->body }}</small>
        </h4>
    </td>
    <td class="text-center hidden-xs hidden-sm">{{ $child->nbDiscussions }}</td>
    <td class="text-center hidden-xs hidden-sm">{{ $child->nbPosts }}</td>
    @if($child->lastPost)
    <td class="hidden-xs hidden-sm">
        <small><a href="{{ route("comment.to", ["id" => $child->lastPost->id]) }}">{{ $child->lastPost->commentable->name }}</a></small><br>
        by <a href="#">{{ $child->lastPost->user->name }}</a><br>
        <small><i class="fa fa-clock-o"></i> {{ $child->lastPost->creation_at }}</small>
    </td>
    @else
    <td class="hidden-xs hidden-sm">&nbsp;</td>
    @endif
</tr>