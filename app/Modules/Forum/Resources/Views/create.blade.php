@extends("layouts.app")

@section("content")
    <div class="container forum">

        <form class="form-horizontal" role="form" action="{{ route("forum.store") }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <legend>Créer une catégorie ou un forum</legend>

            <div class="form-group">
                <label class="control-label" for="name">Nom</label>
                <input type="text" name="name" class="form-control" maxlength="191">
            </div>

            <div class="form-group">
                <label for="parent_id" class="control-label">Forum parent</label>
                <select name="parent_id" id="channel">
                    <option value="">Pas de parent</option>
                    @foreach($channels as $channel)
                        <option value="{{ $channel->id }}" @if(($default->id) && ($channel->id == $default->id)) selected @endif>{{ $channel->name }}</option>
                    @endforeach
                </select>
                <p><i>Les forums prennent les paramètres de leur forum parent</i></p>
            </div>

            <div class="form-group">
                <label class="control-label">Description </label>
                <textarea name="body" class="form-control" rows="3"></textarea>
            </div>

            <div class="text-center">
                <button class="btn btn-primary" type="submit">C'est parti !</button>
            </div>
        </form>

    </div>
@endsection