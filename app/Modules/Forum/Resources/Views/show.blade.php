@extends("layouts.app")

@section("content")
    <div class="container forum">

        <div class="page-header page-heading">
            <h1 class="pull-left">
                {{ $channel->name }}
                <span class="pull-right" style="margin-left: 20px">
                    <a href="{{ route("forum.create") }}">
                        <i class="fa fa-plus"></i>
                    </a>
                    <a href="{{ route("forum.edit", ["slug" => $channel->slug]) }}">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ route("forum.delete", ["id" => $channel->id]) }}">
                        <i class="fa fa-trash"></i>
                    </a>
                    <a href="{{ route("forum.lock", ["id" => $channel->id]) }}">
                        <i class="fa fa-lock"></i>
                    </a>
                </span>
            </h1>
            <ol class="breadcrumb pull-right where-am-i">
                <li><a href="{{ route("forum.index") }}"><i class="fa fa-home"></i></a></li>
                @include("forum::parts.breadcrumbs")
                <li class="active">{{ $channel->name }}</li>
            </ol>
            <div class="clearfix"></div>
        </div>
        <p class="lead">{{ $channel->body }}</p>

        <table class="table forum table-striped">
            <thead>
            <tr>
                <th class="cell-stat"></th>
                <th>
                    <h3>Forums</h3>
                </th>
                <th class="cell-stat text-center hidden-xs hidden-sm">Topics</th>
                <th class="cell-stat text-center hidden-xs hidden-sm">Posts</th>
                <th class="cell-stat-2x hidden-xs hidden-sm">Last Post</th>
            </tr>
            </thead>
            <tbody>
            @if($channel->children->count() > 0)
                @foreach($channel->children as $child)
                    @include("forum::parts.forum")
                @endforeach
            @else
                @include("forum::parts.no_forum")
            @endif
            </tbody>
        </table>

        <table class="table forum table-striped">
            <thead>
            <tr>
                <th class="cell-stat"></th>
                <th>
                    <h3>Discussions</h3>
                </th>
                <th class="cell-stat text-center hidden-xs hidden-sm">Posts</th>
                <th class="cell-stat text-center hidden-xs hidden-sm">Vues</th>
                <th class="cell-stat-2x hidden-xs hidden-sm">Last Post</th>
            </tr>
            </thead>
            <tbody>
            @if($discussions->count() > 0)
                @foreach($discussions as $discussion)
                    @include("forum::parts.discussion")
                @endforeach
            @else
                @include("forum::parts.no_discussion")
            @endif
            </tbody>
        </table>

        {{ $discussions->links() }}
    </div>
@endsection