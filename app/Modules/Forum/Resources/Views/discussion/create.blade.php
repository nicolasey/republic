@extends("layouts.app")

@section("css")
    <link rel="stylesheet" href="http://cdn.wysibb.com/css/default/wbbtheme.css" />
@endsection

@section("content")
    <div class="container forum__posting">
        <div class="row">
            <div class="col">
                <form action="{{ route("discussion.store") }}" method="post" role="form" novalidate>
                    <legend>Poster un nouveau topic</legend>

                    <div class="form-group">
                        <label for="discussion_title"></label>
                        <input type="text" class="form-control" name="discussion_title" placeholder="Titre..." required value="{{ old("discussion_title") }}">
                    </div>

                    <div class="form-group">
                        <textarea title="posting" name="posting" class="form-control" id="posting" required>{{ old("posting") }}</textarea>
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">Tel est mon destin</button>
                    </div>

                    <input type="hidden" name="category_id" value="{{ $channel->id }}">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection