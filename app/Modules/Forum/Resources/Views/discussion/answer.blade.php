@extends("layouts.app")

@section("css")
    <link rel="stylesheet" href="http://cdn.wysibb.com/css/default/wbbtheme.css" />
@endsection

@section("content")
    <div class="container forum__posting">
        <div class="row">
            <div class="col">
                <form action="{{ route("comment.store", ["slug" => $discussion->slug]) }}" method="post" role="form" novalidate>
                    <legend>Répondre</legend>

                    <div class="form-group">
                        <textarea title="posting" name="posting" class="form-control" id="posting" required>{{ old("posting") }}</textarea>
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">Tel est mon destin</button>
                    </div>

                    <input type="hidden" name="discussion_id" value="{{ $discussion->id }}">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection