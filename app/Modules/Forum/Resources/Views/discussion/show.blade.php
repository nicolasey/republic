@extends("layouts.app")

@section("content")
    <div class="container-fluid">
        <h1 class="page-header">
            <i class="fa fa-pencil"></i> {{ $discussion->name }}
            <span class="pull-right">
            <a class="btn btn-default" href="{{ route("forum.show", ["slug" => $discussion->channel->slug]) }}"><i class="fa fa-backward"></i> Back to forum</a>
        </span>
        </h1>
        <ul class="media-list forum">
            @foreach($comments as $comment)
                @include("forum::parts.comment")
            @endforeach
        </ul>

        <div class="form-group text-center">
            <a href="{{ route("comment.create", ["slug" => $discussion->slug]) }}">
                <button class="btn btn-primary">Répondre</button>
            </a>
        </div>

        {{ $comments->links() }}
    </div>
@endsection