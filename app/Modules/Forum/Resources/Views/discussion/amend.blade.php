@extends("layouts.app")

@section("css")
    <link rel="stylesheet" href="http://cdn.wysibb.com/css/default/wbbtheme.css" />
@endsection

@section("content")
    <div class="container forum__posting">
        <div class="row">
            <div class="col">
                <form action="{{ route("comment.update", ["id" => $comment->id]) }}" method="post" role="form" novalidate>
                    <legend>Répondre</legend>

                    <div class="form-group">
                        <textarea title="posting" name="posting" class="form-control" id="posting" required>{{ $comment->body }}</textarea>
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">Tel est mon destin</button>
                    </div>

                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection