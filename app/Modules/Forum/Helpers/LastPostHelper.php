<?php
namespace App\Modules\Forum\Helpers;

use App\Modules\Forum\Models\Channel;
use App\Modules\Forum\Models\Comment;
use App\Modules\Forum\Models\Topic;
use Illuminate\Database\Eloquent\Collection;

class LastPostHelper
{
    /**
     * Set last post for channel and every ancestor
     *
     * @param Channel $channel
     * @param Comment $last_post
     */
    public static function setChannelLastPost(Channel $channel, Comment $last_post)
    {
        $channel->last_post = $last_post->id;
        $channel->save();

        $ancestors = $channel->getAncestors();
        foreach ($ancestors as $ancestor) {
            $ancestor->last_post = $last_post->id;
            $ancestor->save();
        }
    }

    /**
     * Set last post on channels when last post was deleted
     *
     * @param Topic $topic
     */
    public static function setDeleteChannelsLastPost(Topic $topic)
    {
        $channel = Channel::findOrFail($topic->channel_id);
        $actualLastPost = Comment::find($channel->last_post);

        // If post does not exist, it was the deleted post, we must reevaluate
        if (!$actualLastPost) {
            // Find and update last post among discussions and children last post
            $last = LastPostHelper::evaluateChannelLastPost($channel);
            $channel->last_post = $last->id;
            $channel->save();

            $ancestors = $channel->getAncestors()->reverse();

            foreach ($ancestors as $ancestor) {
                $lastPost = $ancestor->lastPost;
                if(!$lastPost) {
                    $lp = LastPostHelper::evaluateChannelLastPost($ancestor);
                    $ancestor->last_post = ($lp) ? $lp->id : null;
                    $ancestor->save();
                }
            }
        }
    }

    /**
     * Evaluate and set last post for given channel
     *
     * @param Channel $channel
     */
    public static function setLastPost(Channel $channel)
    {
        $last = LastPostHelper::evaluateChannelLastPost($channel);
        $channel->last_post = $last->id;
        $channel->save();

        $ancestors = $channel->getAncestors()->reverse();
        foreach ($ancestors as $ancestor) {
            $lastChannelPost = Comment::findOrFail($ancestor->last_post);
            if($lastChannelPost) {
                if($last->created_at < $lastChannelPost->created_at) break;
                $ancestor->last_post = $last->id;
                $ancestor->save();
            }
            else {
                $ancestor->last_post = $last->id;
                $ancestor->save();
            }
        }
    }

    /**
     * Return last post of channel from children and discussions
     *
     * @param Channel $channel
     * @return Comment|null
     */
    private static function evaluateChannelLastPost(Channel $channel)
    {
        $topicLastPost = LastPostHelper::evaluateLastPost($channel->topics);
        $childrenLastPost = LastPostHelper::evaluateLastPost($channel->children);

        if(!($childrenLastPost->id) && ($topicLastPost->id)) return $topicLastPost;
        if(!($topicLastPost->id) && ($childrenLastPost->id)) return $childrenLastPost;
        if (!($topicLastPost->id) && !($childrenLastPost->id)) return null;
        return ($topicLastPost->created_at > $childrenLastPost->created_at) ? $topicLastPost : $childrenLastPost;
    }

    /**
     * Return last created item from a collection
     *
     * @param Collection $items
     * @return Comment
     */
    private static function evaluateLastPost(Collection $items)
    {
        $lastComment = new Comment;
        foreach ($items as $item) {
            $post = $item->lastPost;
            if($post) {
                if(!$lastComment or ($post->created_at > $lastComment->created_at)) $lastComment = $post;
            }
        }
        return $lastComment;
    }

    /**
     * Evaluate and set last post for a topic
     *
     * @param Topic $topic
     */
    public static function setTopicLastPost(Topic $topic)
    {
        $comment = Comment::where([
            "commentable_type" => Topic::class,
            "commentable_id" => $topic->id
        ])->latest()->first();

        $topic->last_post = $comment->id;
        $topic->save();
    }
}