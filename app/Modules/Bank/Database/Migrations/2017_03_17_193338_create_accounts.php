<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("accounts", function (Blueprint $table) {
            $table->increments("id");
            $table->timestamps();
            $table->softDeletes();

            $table->morphs("owner");

            $table->bigInteger("solde")->default(200);
            $table->boolean("locked")->default(false);
            $table->boolean("sugarDaddy")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("accounts");
    }
}
