<?php

namespace App\Modules\Bank\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    /**
     * Account owner entity
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function owner()
    {
        return $this->morphTo("owner");
    }

    /**
     * Test solvency
     *
     * @param $nb
     * @return bool
     */
    public function isSolvent($nb)
    {
        return (($nb >= $this->solde) or $this->sugarDaddy);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions_from()
    {
        return $this->hasMany(Transaction::class, "from");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions_to()
    {
        return $this->hasMany(Transaction::class, "to");
    }

    /**
     * Account pays
     * @param $nb
     */
    public function pay($nb)
    {
        $this->solde -= $nb;
        $this->save();
    }

    /**
     * Account earns
     * @param $nb
     */
    public function earn($nb)
    {
        $this->solde += $nb;
        $this->save();
    }
}
