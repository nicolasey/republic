<?php
namespace App\Modules\Bank\Models\Traits;

use App\User;

trait ApprovalTrait
{
    /**
     * Checks whether a transaction needs an approval to be processed
     * @return bool
     */
    public function requiresApproval()
    {
        // faction to user, user belongs to the faction

        return ($this->isSameFamily());
    }

    /**
     * Check if transaction is being held by two users from same family
     * @return bool
     */
    private function isSameFamily()
    {
        if (is_a(User::class, $this->from->owner()) and is_a(User::class, $this->to->owner()))
        {
            return $this->from()->owner()->family_id == $this->to()->owner()->family_id;
        }
        return false;
    }
}