<?php
namespace App\Modules\Bank\Models\Traits;

use App\Modules\Bank\Models\Account;

trait HasAccount
{
    public function account()
    {
        return $this->morphOne(Account::class, "owner");
    }
}