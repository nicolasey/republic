<?php
namespace App\Modules\Access\Http\Middleware;

use Bouncer;
use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if(auth()->guest() or (! Bouncer::is($request->user())->a($role)))
        {
            session()->flash("danger", "Accès limité");
            return redirect()->back();
        }
        return $next($request);
    }
}