<?php
namespace App\Modules\Access\Models\Traits;

use Bouncer;

trait UserAccessTrait 
{
    /**
     * Give a user its specific access rights
     *
     * @return void
     */
    public static function bootUserAccessTrait()
    {
        static::created(function($model) {
            Bouncer::allow($model)->toOwn($model);
            Bouncer::refreshFor($model);
        });
    } 
}