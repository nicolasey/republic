<?php
/**
 * Created by PhpStorm.
 * User: nicolasey
 * Date: 21/03/2017
 * Time: 19:02
 */

namespace App\Modules\Faction\Listeners;

use App\Modules\Faction\Models\Faction;
use Mpociot\Teamwork\Events\UserLeftTeam;

class RemoveFactionRoleListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserLeftTeam $event
     * @return void
     */
    public function handle(UserLeftTeam $event)
    {
        $faction = Faction::first($event->getTeamId());
        $event->getUser()
            ->retract($faction->slug);
    }
}