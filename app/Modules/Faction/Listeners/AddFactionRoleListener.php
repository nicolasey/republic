<?php
namespace App\Modules\Faction\Listeners;

use App\Modules\Faction\Models\Faction;
use Mpociot\Teamwork\Events\UserJoinedTeam;

class AddFactionRoleListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserJoinedTeam $event
     * @return void
     */
    public function handle(UserJoinedTeam $event)
    {
        $faction = Faction::first($event->getTeamId());
        $event->getUser()
            ->assign($faction->slug);
    }
}