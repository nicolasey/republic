<?php

namespace App\Modules\Faction\Models;

use App\Modules\Bank\Models\Traits\HasAccount;
use App\Traits\SlugTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mpociot\Teamwork\TeamworkTeam;

class Faction extends TeamworkTeam
{
    use SlugTrait, SoftDeletes, HasAccount;

    protected $table = "teams";
}
