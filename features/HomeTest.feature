Feature: Test that behat is ok

   In order to prove that Behat works as intended
   We want to test the homepage for a phrase

   Scenario: Home test
   When I am on the homepage
   Then I should see "Zone 51"

