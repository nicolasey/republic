Feature: Login 

   In order to see my specific data and interact personally with the app
   As users
   We want to log in the application

   Scenario: I register in the app
      Given I am on "/register"
      When I fill in "name" with "Kyp"
      And I fill in "password" with "password"
      And I fill in "password-confirm" with "password"
      And I fill in "email" with "nicolas.sey@gmail.com"
      And I press "Register"
      Then I should be on "/home"
      And I should see "Kyp" in the "navbar" element

   Scenario: I log out from the app
      Given I am on "/home"
      When I press "Kyp"
      And I press "Logout"
      Then I should be on the homepage
      And I should not see "Kyp"

   Scenario: I log into the app
      Given I am on "/login"
      When I fill in "name" with "Kyp"
      And I fill in "password" with "password"
      And I press "Login"
      Then I should be on "/home"
      And I should see "Kyp" in the "navbar" element


