@if(Session::has("danger"))
    @component("components.alert")
        @slot("type")
            danger
        @endslot

        {{ Session::get("danger") }}
    @endcomponent
@endif

@if(Session::has("warning"))
    @component("components.alert")
        @slot("type")
            warning
        @endslot

        {{ Session::get("warning") }}
    @endcomponent
@endif

@if(Session::has("success"))
    @component("components.alert")
        @slot("type")
            success
        @endslot

        {{ Session::get("success") }}
    @endcomponent
@endif

