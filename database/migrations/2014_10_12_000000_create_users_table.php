<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string("slug");
            $table->string('email');
            $table->string('password');
            $table->string("avatar")->default('default.png');
            $table->string("validation_token")->nullable();
            $table->dateTime("logged_at")->nullable();
            $table->dateTime("last_login")->nullable();
            $table->boolean("locked")->default(false);
            $table->boolean("active")->default(false);
            $table->boolean("staff")->default(false);
            $table->boolean("sleep")->default(false);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
