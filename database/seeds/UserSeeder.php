<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->create([
            "name" => "Kyp"
        ]);
        factory(\App\User::class)->create([
            "name" => "Admin"
        ]);
        factory(\App\User::class)->create([
            "name" => "Meujeu"
        ]);
        factory(\App\User::class)->create([
            "name" => "Joueur"
        ]);
    }
}
