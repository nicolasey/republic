<?php

use Illuminate\Database\Seeder;
use App\Modules\Forum\Seeds\ChannelSeeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ChannelSeeder::class);
    }
}
